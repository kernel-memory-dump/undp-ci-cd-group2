def sum(first_number, second_number):
    if first_number < 0 or second_number < 0:
        return 0
    
    return first_number + second_number
