import example
import sys

failed_tests = []
passed_tests = []

def add_test_regular_numbers():
    expected = 4
    result = example.sum(2, 2)
    if result != expected:
        failed_tests.append(f'add_test_regular_numbers FAILED: called with (2,2), expected {expected} but got {result} instead')
    else:
        passed_tests.append(f'add_test_regular_numbers PASS')
def test_main():
    # CALL ALL TEST SCENARIOS / test cases
    add_test_regular_numbers()
    # ... 
    # CHECK IF ANY OF THEM FAILED?
    for failed_test in failed_tests:
        print(failed_test)
        sys.exit(1)
    
    # if it got to there, it means
    # there were no failed tests
    for passed_test in passed_tests:
        print(passed_test)

    print('ALL TESTS OK')
    sys.exit(0)
    




test_main()